package ch.cern.todo.controller;

import ch.cern.todo.dto.TaskCategoryDTO;
import ch.cern.todo.dto.TaskDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class TaskControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testCreateCategoryAndTasks() throws Exception {
        // Create Category
        TaskCategoryDTO category = new TaskCategoryDTO(null, "Test Category", "Description", null);
        String categoryJson = objectMapper.writeValueAsString(category);

        MvcResult result = mockMvc.perform(post("/categories")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(categoryJson))
                .andExpect(status().isOk())
                .andReturn();

        TaskCategoryDTO createdCategory = objectMapper.readValue(result.getResponse().getContentAsString(), TaskCategoryDTO.class);


        TaskDTO task1 = new TaskDTO(null, "Task 1", "Description 1", null, createdCategory.getCategoryId());
        TaskDTO task2 = new TaskDTO(null, "Task 2", "Description 2", null, createdCategory.getCategoryId());

        String taskJson1 = objectMapper.writeValueAsString(task1);

        mockMvc.perform(post("/tasks")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(taskJson1))
                .andExpect(status().isOk());

        String taskJson2 = objectMapper.writeValueAsString(task2);

        mockMvc.perform(post("/tasks")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(taskJson2))
                .andExpect(status().isOk());

    }

    @Test
    public void testCreateSecondCategoryAndTasks() throws Exception {
        // Create a second category with two tasks using the same approach as above
        // Assert fetch both categories and that they each have 2 tasks
    }

    @Test
    public void testUpdateCategoryAndTask() throws Exception {
        // Create category and tasks
        // Update category and task using PUT requests
        // Assert changes
    }

    @Test
    public void testDeleteTask() throws Exception {
        // Create a category and task
        // Delete task using DELETE
        // Assert task is no longer found
    }
}
