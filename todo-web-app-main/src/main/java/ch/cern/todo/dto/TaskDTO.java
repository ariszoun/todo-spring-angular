package ch.cern.todo.dto;

import java.sql.Timestamp;
import java.util.Objects;

public class TaskDTO {
    private Long taskId;
    private String taskName;
    private String taskDescription;
    private Timestamp deadline;
    private Long categoryId;

    public TaskDTO() {
    }

    public TaskDTO(Long taskId, String taskName, String taskDescription, Timestamp deadline, Long categoryId) {
        this.taskId = taskId;
        this.taskName = taskName;
        this.taskDescription = taskDescription;
        this.deadline = deadline;
        this.categoryId = categoryId;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public Timestamp getDeadline() {
        return deadline;
    }

    public void setDeadline(Timestamp deadline) {
        this.deadline = deadline;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskDTO taskDTO = (TaskDTO) o;
        return Objects.equals(taskId, taskDTO.taskId) && Objects.equals(taskName, taskDTO.taskName) && Objects.equals(taskDescription, taskDTO.taskDescription) && Objects.equals(deadline, taskDTO.deadline) && Objects.equals(categoryId, taskDTO.categoryId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskId, taskName, taskDescription, deadline, categoryId);
    }
}
