package ch.cern.todo.service;

import ch.cern.todo.dto.TaskDTO;
import ch.cern.todo.model.Task;
import ch.cern.todo.repository.TaskCategoryRepository;
import ch.cern.todo.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



@Service
public class TaskService {

    private final TaskRepository taskRepository;
    private final TaskCategoryRepository taskCategoryRepository;

    @Autowired
    public TaskService(TaskRepository taskRepository, TaskCategoryRepository taskCategoryRepository) {
        this.taskRepository = taskRepository;
        this.taskCategoryRepository = taskCategoryRepository;
    }

    @Transactional(readOnly = true)
    public Page<TaskDTO> findAllTasks(Pageable pageable, String sortBy, String direction) {
        Sort sort = direction.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending() : Sort.by(sortBy).descending();
        Pageable sortedPageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
        return taskRepository.findAll(sortedPageable)
                .map(this::entityToDTO);
    }

    @Transactional(readOnly = true)
    public TaskDTO findTaskById(Long id) {
        return taskRepository.findById(id)
                .map(this::entityToDTO)
                .orElse(null);
    }

    @Transactional
    public TaskDTO saveTask(TaskDTO dto) {
        Task task = dtoToEntity(dto);
        task = taskRepository.save(task);
        return entityToDTO(task);
    }

    @Transactional
    public void deleteTask(Long id) {
        taskRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public Page<TaskDTO> findTasksByCategoryId(Long categoryId, Pageable pageable, String sortBy, String direction) {
        Sort sort = direction.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending() : Sort.by(sortBy).descending();
        Pageable sortedPageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
        return taskRepository.findByCategoryId(categoryId, sortedPageable)
                .map(this::entityToDTO);
    }

    private TaskDTO entityToDTO(Task task) {
        TaskDTO dto = new TaskDTO();
        dto.setTaskId(task.getTaskId());
        dto.setTaskName(task.getTaskName());
        dto.setTaskDescription(task.getTaskDescription());
        dto.setDeadline(task.getDeadline());
        dto.setCategoryId(task.getCategory() != null ? task.getCategory().getCategoryId() : null);
        return dto;
    }

    private Task dtoToEntity(TaskDTO dto) {
        Task task = new Task();
        task.setTaskId(dto.getTaskId());
        task.setTaskName(dto.getTaskName());
        task.setTaskDescription(dto.getTaskDescription());
        task.setDeadline(dto.getDeadline());
        task.setCategory(taskCategoryRepository.findById(dto.getCategoryId()).orElse(null));
        return task;
    }
}
