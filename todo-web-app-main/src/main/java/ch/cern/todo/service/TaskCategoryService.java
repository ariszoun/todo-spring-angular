package ch.cern.todo.service;

import ch.cern.todo.dto.TaskCategoryDTO;
import ch.cern.todo.dto.TaskDTO;
import ch.cern.todo.model.Task;
import ch.cern.todo.model.TaskCategory;
import ch.cern.todo.repository.TaskCategoryRepository;
import jakarta.persistence.EntityNotFoundException;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TaskCategoryService {

    private final TaskCategoryRepository taskCategoryRepository;

    @Autowired
    public TaskCategoryService(TaskCategoryRepository taskCategoryRepository) {
        this.taskCategoryRepository = taskCategoryRepository;
    }

    @Transactional(readOnly = true)
    public List<TaskCategoryDTO> findAllCategories() {
        return taskCategoryRepository.findAllWithTasks().stream()
                .map(this::entityToDTO)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public TaskCategoryDTO findCategoryById(Long id) {
        return taskCategoryRepository.findByIdWithTasks(id)
                .map(this::entityToDTO)
                .orElse(null);
    }

    @Transactional
    public TaskCategoryDTO saveCategory(TaskCategoryDTO dto) {
        if (dto.getCategoryId() != null) {
            // load existing category and update it
            TaskCategory existingCategory = taskCategoryRepository.findById(dto.getCategoryId())
                    .orElseThrow(() -> new EntityNotFoundException("Category not found with id " + dto.getCategoryId()));
            existingCategory.setCategoryName(dto.getCategoryName());
            existingCategory.setCategoryDescription(dto.getCategoryDescription());

            return entityToDTO(taskCategoryRepository.save(existingCategory));
        } else {
            TaskCategory category = dtoToEntity(dto);
            return entityToDTO(taskCategoryRepository.save(category));
        }
    }

    @Transactional
    public void deleteCategory(Long id) {
        taskCategoryRepository.deleteById(id);
    }

    private TaskDTO taskToTaskDTO(Task task) {
        TaskDTO dto = new TaskDTO();
        dto.setTaskId(task.getTaskId());
        dto.setTaskName(task.getTaskName());
        dto.setTaskDescription(task.getTaskDescription());
        dto.setDeadline(task.getDeadline());
        if (task.getCategory() != null) {
            dto.setCategoryId(task.getCategory().getCategoryId());
        }
        return dto;
    }

    private TaskCategoryDTO entityToDTO(TaskCategory category) {
        TaskCategoryDTO dto = new TaskCategoryDTO();
        dto.setCategoryId(category.getCategoryId());
        dto.setCategoryName(category.getCategoryName());
        dto.setCategoryDescription(category.getCategoryDescription());

        if (category.getTasks() != null && Hibernate.isInitialized(category.getTasks())) {
            List<TaskDTO> taskDTOs = category.getTasks().stream()
                    .map(this::taskToTaskDTO)
                    .collect(Collectors.toList());
            dto.setTasks(taskDTOs);
        } else {
            dto.setTasks(new ArrayList<>());
        }

        return dto;
    }

    private TaskCategory dtoToEntity(TaskCategoryDTO dto) {
        TaskCategory category = new TaskCategory();
        category.setCategoryId(dto.getCategoryId());
        category.setCategoryName(dto.getCategoryName());
        category.setCategoryDescription(dto.getCategoryDescription());
        return category;
    }
}
