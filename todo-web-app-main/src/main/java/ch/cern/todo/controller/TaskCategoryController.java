package ch.cern.todo.controller;

import ch.cern.todo.dto.TaskCategoryDTO;
import ch.cern.todo.service.TaskCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/categories")
public class TaskCategoryController {

    private final TaskCategoryService taskCategoryService;

    @Autowired
    public TaskCategoryController(TaskCategoryService taskCategoryService) {
        this.taskCategoryService = taskCategoryService;
    }

    @GetMapping
    public List<TaskCategoryDTO> getAllCategories() {
        return taskCategoryService.findAllCategories();
    }

    @GetMapping("/{id}")
    public ResponseEntity<TaskCategoryDTO> getCategoryById(@PathVariable Long id) {
        TaskCategoryDTO categoryDTO = taskCategoryService.findCategoryById(id);
        return categoryDTO != null ? ResponseEntity.ok(categoryDTO) : ResponseEntity.notFound().build();
    }

    @PostMapping
    public TaskCategoryDTO createCategory(@RequestBody TaskCategoryDTO categoryDTO) {
        return taskCategoryService.saveCategory(categoryDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TaskCategoryDTO> updateCategory(@PathVariable Long id, @RequestBody TaskCategoryDTO categoryDTO) {
        categoryDTO.setCategoryId(id);
        TaskCategoryDTO updatedDTO = taskCategoryService.saveCategory(categoryDTO);
        return ResponseEntity.ok(updatedDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCategory(@PathVariable Long id) {
        taskCategoryService.deleteCategory(id);
        return ResponseEntity.ok().build();
    }

}
