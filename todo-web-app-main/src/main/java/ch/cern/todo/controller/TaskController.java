package ch.cern.todo.controller;
import ch.cern.todo.dto.TaskDTO;
import ch.cern.todo.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/tasks")
public class TaskController {

    private final TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping
    public ResponseEntity<Page<TaskDTO>> getAllTasks(
            @PageableDefault(size = 10) Pageable pageable,
            @RequestParam Optional<String> sortBy,
            @RequestParam Optional<String> direction) {
        return ResponseEntity.ok(taskService.findAllTasks(pageable, sortBy.orElse("taskName"), direction.orElse("asc")));
    }

    @GetMapping("/{id}")
    public ResponseEntity<TaskDTO> getTaskById(@PathVariable Long id) {
        TaskDTO taskDTO = taskService.findTaskById(id);
        return taskDTO != null ? ResponseEntity.ok(taskDTO) : ResponseEntity.notFound().build();
    }

    @PostMapping
    public TaskDTO createTask(@RequestBody TaskDTO taskDTO) {
        return taskService.saveTask(taskDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TaskDTO> updateTask(@PathVariable Long id, @RequestBody TaskDTO taskDTO) {
        taskDTO.setTaskId(id);
        TaskDTO updatedDTO = taskService.saveTask(taskDTO);
        return ResponseEntity.ok(updatedDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTask(@PathVariable Long id) {
        taskService.deleteTask(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/category/{categoryId}")
    public ResponseEntity<Page<TaskDTO>> findTasksByCategoryId(
            @PathVariable Long categoryId,
            @PageableDefault(size=10) Pageable pageable,
            @RequestParam Optional<String> sortBy,
            @RequestParam Optional<String> direction) {
        return ResponseEntity.ok(taskService.findTasksByCategoryId(categoryId, pageable, sortBy.orElse("taskName"), direction.orElse("asc")));
    }
}
