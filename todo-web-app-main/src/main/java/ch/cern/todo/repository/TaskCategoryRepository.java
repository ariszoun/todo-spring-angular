package ch.cern.todo.repository;

import ch.cern.todo.model.TaskCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskCategoryRepository extends JpaRepository<TaskCategory, Long> {

    @Query("SELECT DISTINCT c FROM TaskCategory c LEFT JOIN FETCH c.tasks WHERE c.categoryId = :id")
    Optional<TaskCategory> findByIdWithTasks(@Param("id") Long id);

    @Query("SELECT DISTINCT c FROM TaskCategory c LEFT JOIN FETCH c.tasks")
    List<TaskCategory> findAllWithTasks();

}
