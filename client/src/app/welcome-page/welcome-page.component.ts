import { Component, OnInit, ViewChild } from '@angular/core';
import { Task } from '../models/task.dto';
import { TaskService } from "../services/task.service";
import { TaskModalComponent } from '../add-task-modal/task-modal.component';
import {CommonModule, NgForOf, NgIf} from "@angular/common";
import {TaskCategory} from "../models/category.dto";
import {CategoryService} from "../services/category.service";
import {RouterModule} from "@angular/router";

@Component({
  selector: 'app-welcome-page',
  templateUrl: './welcome-page.component.html',
  styleUrls: ['./welcome-page.component.css'],
  standalone: true,
  imports: [
    RouterModule,
    NgForOf,
    NgIf,
    TaskModalComponent,
    CommonModule
  ]
})
export class WelcomePageComponent implements OnInit {
  tasks: Task[] = [];
  categories: TaskCategory[] = [];
  selectedCategoryId: number | undefined;
  currentPage: number = 1;
  itemsPerPage: number = 10;
  totalItems: number = 0;
  totalElements: number | undefined;
  totalPages: number = 0;
  sortDirection: string = 'asc';
  sortBy: string = 'taskName';

  @ViewChild(TaskModalComponent) addTaskModal!: TaskModalComponent;

  constructor(private taskService: TaskService, private categoryService: CategoryService) {}

  ngOnInit(): void {
    this.loadTasks();
    this.loadCategories();
  }

  loadTasks(): void {
    const params = {
      page: this.currentPage,
      size: this.itemsPerPage,
      categoryId: this.selectedCategoryId,
      sortBy: this.sortBy,
      direction: this.sortDirection
    };
    this.fetchTasks(params);
  }

  fetchTasks(params: {page: number; size: number; categoryId?: number, sortBy?: string, direction?: string }): void {
    if (params.categoryId) {
      this.taskService.findTasksByCategoryId(params.categoryId, params.page, params.size, params.sortBy, params.direction)
        .subscribe(response => {
          this.tasks = response.content;
          this.totalItems = response.totalElements;
          this.updateTotalPages();
        });
    } else {
      this.taskService.getAllTasks(params.page, params.size, params.sortBy, params.direction)
        .subscribe(response => {
          this.tasks = response.content;
          this.totalItems = response.totalElements;
          this.updateTotalPages();
        });
    }
  }

  loadCategories() {
    this.categoryService.getAllCategories().subscribe(data => this.categories = data);
  }

  openModal(task?: Task): void {
    this.addTaskModal.openModal(task);
  }

  deleteTask(taskId: number | undefined): void {
    if (taskId) {
      this.taskService.deleteTask(taskId).subscribe(() => this.loadTasks());
    }
  }

  onCategoryChange(categoryId: string) {
    this.selectedCategoryId = categoryId ? Number(categoryId) : undefined;
    this.currentPage = 1;
    this.loadTasks();
  }

  onPageChange(page: number): void {
    this.currentPage = page;
    this.loadTasks();
  }

  private updateTotalPages(): void {
    this.totalPages = Math.ceil(this.totalItems / this.itemsPerPage);
  }

  toggleSort(sortBy: string): void {
    if (this.sortBy === sortBy) {
      this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
    } else {
      this.sortBy = sortBy;
      this.sortDirection = 'asc';
    }
    this.loadTasks();
  }

}
