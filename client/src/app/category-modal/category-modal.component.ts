import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TaskCategory } from '../models/category.dto';
import {FormsModule} from "@angular/forms";
import {NgStyle} from "@angular/common";

@Component({
  selector: 'app-category-modal',
  templateUrl: './category-modal.component.html',
  styleUrls: ['./category-modal.component.css'],
  standalone: true,
  imports: [
    FormsModule,
    NgStyle,
  ]
})
export class CategoryModalComponent {
  @Input() category: TaskCategory = { categoryName: '', categoryDescription: '' };
  @Output() save: EventEmitter<TaskCategory> = new EventEmitter();
  @Output() close: EventEmitter<void> = new EventEmitter();
  showModal = false;

  constructor() {
  }

  openModal(category?: TaskCategory): void {
    this.showModal = true;
    this.category = category ? { ...category } : { categoryName: '', categoryDescription: '' };
  }

  onSave(): void {
    this.save.emit(this.category);
  }

  onClose(): void {
    this.showModal = false;
    this.close.emit();
  }
}
