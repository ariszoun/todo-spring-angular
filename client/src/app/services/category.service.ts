import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TaskCategory } from '../models/category.dto';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  private apiUrl = 'http://localhost:8080/api/v1/categories';

  constructor(private http: HttpClient) {}

  getAllCategories(): Observable<TaskCategory[]> {
    return this.http.get<TaskCategory[]>(this.apiUrl);
  }

  getCategoryById(id: number): Observable<TaskCategory> {
    return this.http.get<TaskCategory>(`${this.apiUrl}/${id}`);
  }

  addCategory(category: TaskCategory): Observable<TaskCategory> {
    return this.http.post<TaskCategory>(this.apiUrl, category);
  }

  editCategory(category: TaskCategory): Observable<TaskCategory> {
    return this.http.put<TaskCategory>(`${this.apiUrl}/${category.categoryId}`, category);
  }

  deleteCategory(id: number): Observable<void> {
    return this.http.delete<void>(`${this.apiUrl}/${id}`);
  }
}
