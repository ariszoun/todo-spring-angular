import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Task } from '../models/task.dto';


@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private apiUrl = 'http://localhost:8080/api/v1/tasks';

  constructor(private http: HttpClient) {}

  getAllTasks(page: number = 1, size: number = 10, sortBy: string = 'taskName', direction: string = 'asc'): Observable<any> {
    let params = new HttpParams()
        .set('page', page-1)
        .set('size', size.toString())
        .set('sort', `${sortBy},${direction}`);
    return this.http.get<any>(this.apiUrl, {params});
  }

  getTaskById(id: number): Observable<Task> {
    return this.http.get<Task>(`${this.apiUrl}/${id}`);
  }

  addTask(task: Task): Observable<Task> {
    return this.http.post<Task>(this.apiUrl, task);
  }

  editTask(task: Task): Observable<Task> {
    return this.http.put<Task>(`${this.apiUrl}/${task.taskId}`, task);
  }

  deleteTask(id: number): Observable<void> {
    return this.http.delete<void>(`${this.apiUrl}/${id}`);
  }

  findTasksByCategoryId(categoryId: number, page: number = 1, size: number = 10, sortBy: string = 'taskName', direction: string = 'asc'): Observable<any> {
    let params = new HttpParams()
        .set('page', page-1)
        .set('size', size.toString())
        .set('sort', `${sortBy},${direction}`);
    return this.http.get<any>(`${this.apiUrl}/category/${categoryId}`, {params});
  }
}
