export interface Task {
  taskId?: number;
  taskName: string;
  taskDescription: string;
  deadline: string; // Using ISO string format for dates
  categoryId: number;
}
