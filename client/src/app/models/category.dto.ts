import { Task } from './task.dto';

export interface TaskCategory {
  categoryId?: number;
  categoryName: string;
  categoryDescription: string;
  tasks?: Task[];
}
