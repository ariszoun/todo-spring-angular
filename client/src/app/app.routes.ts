import { Routes } from '@angular/router';
import { WelcomePageComponent } from '../app/welcome-page/welcome-page.component';
import {ManageCategoriesComponent} from "./manage-categories/manage-categories.component";


export const routes: Routes = [
  { path: '', redirectTo: '/welcome', pathMatch: 'full' },
  { path: 'welcome', component: WelcomePageComponent },
  { path: 'manage-categories', component: ManageCategoriesComponent }

];


