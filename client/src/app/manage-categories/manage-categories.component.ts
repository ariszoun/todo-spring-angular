import {Component, OnInit, ViewChild} from '@angular/core';
import { CategoryService } from '../services/category.service';
import { TaskCategory } from '../models/category.dto';
import { CategoryModalComponent } from '../category-modal/category-modal.component';
import { NgForOf, NgIf } from '@angular/common';
import {RouterLink} from "@angular/router";

@Component({
  selector: 'app-manage-categories',
  templateUrl: './manage-categories.component.html',
  styleUrls: ['./manage-categories.component.css'],
  standalone: true,
  imports: [CategoryModalComponent, NgForOf, NgIf, RouterLink]
})
export class ManageCategoriesComponent implements OnInit {
  categories: TaskCategory[] = [];
  @ViewChild(CategoryModalComponent) categoryModal!: CategoryModalComponent;

  constructor(private categoryService: CategoryService) {}

  ngOnInit(): void {
    this.loadCategories();
  }

  loadCategories(): void {
    this.categoryService.getAllCategories().subscribe(categories => this.categories = categories);
  }

  openModal(category?: TaskCategory): void {
    if (this.categoryModal) {
      this.categoryModal.openModal(category);
    }
  }

  // Use for save or edit a category
  saveOrEditCategory(category: TaskCategory): void {
    const operation = category.categoryId
        ? this.categoryService.editCategory(category)
        : this.categoryService.addCategory(category);

    operation.subscribe(() => {
      this.loadCategories();
      this.categoryModal.onClose();
    });
  }

  deleteCategory(id: number): void {
    this.categoryService.deleteCategory(id).subscribe(() => this.loadCategories());
  }
}
