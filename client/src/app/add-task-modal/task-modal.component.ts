import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {FormsModule} from '@angular/forms';
import { TaskService } from '.././services/task.service';
import {Task} from "../models/task.dto";
import {NgClass, NgForOf, NgIf, NgStyle} from "@angular/common";
import {TaskCategory} from "../models/category.dto";
import {CategoryService} from "../services/category.service";


@Component({
  selector: 'app-add-task-modal',
  templateUrl: './task-modal.component.html',
  styleUrls: ['./task-modal.component.css'],
  standalone: true,
  imports: [
    FormsModule,
    NgClass,
    NgIf,
    NgStyle,
    NgForOf

  ]
})
export class TaskModalComponent implements OnInit {
  @Input() task: Task = { taskName: '', taskDescription: '', deadline: '', categoryId: 0 };
  @Output() modalClose: EventEmitter<void> = new EventEmitter<void>();
  showModal = false;
  categories: TaskCategory[] = [];

  constructor(private taskService: TaskService, private categoryService: CategoryService) {}

  ngOnInit(): void {
    this.fetchCategories();
  }

  // Use for save or edit a task
  saveOrEditTask(): void {
    const operation = this.task.taskId ? this.taskService.editTask(this.task) : this.taskService.addTask(this.task);
    operation.subscribe({
      next: (result) => {
        this.closeModal();
        this.modalClose.emit();
      },
      error: (error) => console.error('Failed to save the task', error)
    });
  }

  fetchCategories(): void {
    this.categoryService.getAllCategories().subscribe(categories => this.categories = categories);
  }

  openModal(task?: Task): void {
    this.fetchCategories();
    if (task) {
      this.task = {...task}; // Clone the task to edit
      this.showModal = true;
    } else {
      this.task = { taskName: '', taskDescription: '', deadline: '', categoryId: 0 }; // Reset for a new task
      this.showModal = true;
    }
  }

  closeModal(): void {
    this.showModal = false;
    this.modalClose.emit();
  }

}
