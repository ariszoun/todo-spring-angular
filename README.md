# Task Management Application

## Introduction

This project is a web-based task management application. The application allows users to access, add, edit, and delete tasks. Tasks are organized into categories, which users can also manage by adding, editing, and deleting categories. This application is built using Angular for the frontend and Spring Boot for the backend.

## Backend

The backend of this application is developed using **Spring Boot**.

### Key Dependencies

- Spring Boot Version: `3.2.1`
- Spring Boot Starter Data JPA
- Spring Boot Starter Web
- H2 Database for runtime data storage
- JUnit Platform for testing

### Build Tool

- Gradle

## Frontend

The frontend of this application is developed using **Angular**.

### Key Dependencies

- Angular Version: `17.3.0`
- Bootstrap: `5.3.3` for styling
- RxJS: `~7.8.0` for reactive programming

### Build Tool

- Angular CLI: `17.3.3`